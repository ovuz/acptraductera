var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// kullanıcı şeması
var userSchema = mongoose.Schema({
    _id: String,
    password: String,
    addedproducts: [Number],
    comments: [{
        product: String,
        text: String
    }]
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', userSchema);