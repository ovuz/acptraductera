var mongoose = require('mongoose');
// yapılan aramaların şeması
var searchSchema = mongoose.Schema({
    productId: Number,
    productName: String
});
module.exports = mongoose.model('Search', searchSchema);