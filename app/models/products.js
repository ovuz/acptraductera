var mongoose = require('mongoose');

var connection = mongoose.createConnection(require(__basedir + '/config/database.js').url);
autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(connection);

// ürün şeması
var productSchema = mongoose.Schema({
    name: {type: String, text: true},
    price: String,
    poster: String,
    comments: [{
        text: String,
        poster: String
    }]
});

// productId diye kendini artıran bi index. database sorgularını baya rahatlatıyor
productSchema.plugin(autoIncrement.plugin, { model: 'Product', field: 'productId' });

module.exports = mongoose.model('Product', productSchema);