// db modülü & konfigürasyon
var mongoose = require('mongoose');
mongoose.connect(require(__basedir + '/config/database.js').url);

// schema modülleri
var Product = require(__basedir + '/app/models/products');
var User = require(__basedir + '/app/models/users');
var Search = require(__basedir + '/app/models/searches');


module.exports = function(app, passport) {
    
    // çağıran: header.pug
    // son 12 arama ve son 12 ürünü geçirerek ana sayfayı render et
    app.get('/', function(req, res) {
        Product.find({}, {}, { sort: { 'productId' : -1 } }).limit(12).exec(function(err, results) {
            Search.find({}, {}, { sort: { '_id' : -1 } }).limit(12).exec(function(err, lastsearched) {
                res.render('main', {req : req, lastadded: results, lastsearched: lastsearched});
            });
        });
    });
    
    // çağıran: header.pug
    // kayıt sayfası get render
    app.get('/kayit', function(req, res) {
        if(req.isAuthenticated()) 
            res.redirect('/');
        else res.render('register', {req : req, message: req.flash('signupMessage')});
    });
    
    // çağıran: register.pug
    // kayıt sayfasındaki formdan aldıklarını config/passport.js'te işle
    app.post('/kayit', passport.authenticate('local-signup', {
        successRedirect : '/', 
        failureRedirect : '/kayit', 
        failureFlash : true
    }));
    
    // çağıran: header.pug
    // giriş sayfası get render
    app.get('/giris', function(req, res) {
        if(req.isAuthenticated()) 
            res.redirect('/');
        else res.render('login', {req : req, message: req.flash('loginMessage')});
    });

    // çağıran: login.pug
    // giriş sayfasındaki formdan aldıklarını config/passport.js'te işle
    app.post('/giris', passport.authenticate('local-login', {
        successRedirect : '/', 
        failureRedirect : '/giris', 
        failureFlash : true
    }));
    
    // çağıran: header.pug
    // yeni ürün get render
    app.get('/yeniurun', function(req, res) {
        if(req.isAuthenticated()) 
            res.render('newproduct', {req : req});
        else res.redirect('/');
    });
    
    // çağıran: newproduct.pug
    // yeni ürün sayfasındaki formdan gelenleri veritabanına yaz
    app.post('/yeniurun', function(req, res) {
        var newProduct = new Product();
        newProduct.name = req.body.name;
        newProduct.price = req.body.price;
        newProduct.poster = req.user._id;
        Product.nextCount(function(err,count){
            newProduct.save();
            User.updateOne({_id:req.user._id}, { $push: {addedproducts: count}}, function(err){
                if (err) console.log("product count hatası");
                else res.redirect("/urun/"+count);
            });     
        });
    });
    
    //çağıran: header.pug
    app.get('/cikis', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    
    // çağıran main.pug
    // ana sayfada butona basınca aktive olan request. ürün geçerliyse alttakine yönlendiriyor
    // bu geçici bi metoddu, pug'a taşıyacaktım bu kısmı unutmuşum
    app.post('/ara', function(req, res) {
        if (req.body.product) res.redirect('/ara/'+req.body.product);
        else res.redirect('/');
    });
    
    // buradaki mongodb'nin yerleşik textScore algoritması gibi bi şey,
    // ama iyi değil, değişik şeyleri denemek lazım
    app.get('/ara/:query', function(req, res) {
        Product.find({ $text : { $search : req.params.query } }, 
            { score : { $meta: "textScore" } })
            .sort({ score : { $meta : 'textScore' } })
            .exec(function(err, results) {
                res.render('searchresults', {results: results, req: req});
        });
    });
    
    // çağıran: main.pug searchresults.pug
    // bu metod iki şekilde çağırılıyor. /urun/xxx veya /urun/xxx?a=1 şeklinde
    // /urun/xxx olarak çağırılınca normal ürün sayfasını render ediyor
    // /urun/xxx/?a=1 olarak çağırılınca ürünü arananlar tablosuna ekleyip öyle render ediyor
    app.get('/urun/:id', function(req, res) {
        Product.findOne({'productId': req.params.id}, function(err, product){
            if(err) console.log("urun/id database sorunu");
            else if(!product) res.redirect('/');
            else{ 
                if(req.query.a){ 
                    var newSearch = new Search();
                    newSearch.productId = req.params.id;
                    newSearch.productName = product.name;
                    newSearch.save();
                }
                res.render('product', {product: product, req: req}); 
            }
        });
    });
    
    // çağıran: product.pug 
    // yorumlar şimdilik hem ürün hem de kullanıcı tablolarına ekleniyor, sonra optimize edilebilir
    app.post('/yorum/:id', function(req, res) {
        Product.updateOne({productId: req.params.id}, { $push: { comments: 
                {text: req.body.comment, poster: req.user._id}}}, function(err){
            if(err) console.log("yorum/id yorum database sorunu");
            else User.updateOne({_id: req.user._id}, { $push: { comments: 
                    {text: req.body.comment, product: req.params.id}}}, function(err){
                if(err) console.log("yorum/id kullanıcı database sorunu");
                else res.redirect('/urun/'+req.params.id);
            });
        });
    });
    
    // çağıran: header.pug product.pug searchresults.pug 
    app.get('/kullanici/:id', function(req, res) {
        User.findOne({'_id': req.params.id}, function(err, user){
            if(err) console.log("kullanıcı/id database sorunu");
            else if(!user) res.redirect('/');
            else res.render('user',{user: user, req: req}); 
        });
    });
    
    // çağıran: main.pug
    // arama kutusunun gönderdiği veriye göre veritabanından bilgi çekerek geri pass ediyor
    app.post('/anlikarama', function(req, res){
        Product.find({'name': { $regex: req.body.query, $options: 'i' } })
            .exec(function(err, results) {
                res.send(results);
        });
        
    });
    
};
    