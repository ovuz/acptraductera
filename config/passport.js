// passport modülünün konfigürasyonu. giriş ve kayıt işlemleri burada

var LocalStrategy   = require('passport-local').Strategy;
var User            = require('../app/models/users');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'id',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, id, password, done) {
        process.nextTick(function() {
            User.findOne({ '_id' :  id }, function(err, user) {
                if (err)
                    return done(err);
                if (user) {
                    return done(null, false, req.flash('signupMessage', 'Kullanıcı adı kullanılıyor.'));
                } else {
                    var newUser = new User();
                    newUser._id    = id;
                    newUser.password = newUser.generateHash(password);
                    newUser.save(function(err) {
                        if (err) throw err;
                        return done(null, newUser);
                    });
                }
            });    
        });
    }));
    passport.use('local-login', new LocalStrategy({
        usernameField : 'id',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, id, password, done) {
        User.findOne({ '_id' :  id }, function(err, user) {
            if (err)
                return done(err);
            if (!user)
                return done(null, false, req.flash('loginMessage', 'Böyle bir kullanıcı yok.')); 
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Yanlış şifre.')); 
            return done(null, user);
        });
    }));
};

