// node modülleri
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var passport = require('passport');
var flash    = require('connect-flash');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');


require('./config/passport')(passport);

// root klasör yolu global olarak __basedir diye çağırınca gelecek
global.__basedir = __dirname;

// express app eklentileri ve konfigürasyonu
app.use(morgan('dev')); 
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static(__basedir + '/public'));

app.set('views', __basedir + '/views');
app.set('view engine', 'pug');

app.use(session({ secret: 'sepetsepetmertsepeti', resave: true, saveUninitialized: true, cookie: {maxAge: 31536000000 } }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// route scripti
require('./app/routes.js')(app, passport);


app.listen(port);
console.log('Port: ' + port);

